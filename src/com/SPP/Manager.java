package com.SPP;

import java.util.Scanner;

public class Manager {
    private Scanner scanner;
    private QuestionsList questionsList;
    private User user;

    public void start(){
        scanner = new Scanner(System.in);
        questionsList = new QuestionList();

        System.out.println("Quel est votre statut?");
        System.out.println("\tAdministrateur, tapez 1");
        System.out.println("\tParticipant, tapez 2");
        System.out.println();

        readStatus();

    }

    private void readStatus(){
        int userChar = scanner.nextLine().charAt(0) - '0';
        switch (userChar){
            case 1 :
                startAdministrator();
                break;
            case 2 :
                startParticipant();
                break;
        }
    }

    private void startAdministrator() {
        for(;;){
            questionsList.addQuestion();
        }
    }

    private void startParticipant() {
        System.out.println("Bienvenue dans ce questionnaire de satisfaction");
        user = new User();

        while(!questionsList.isEmpty()){
            questionsList.nextQuestion();
        }


        questionsList.getSatisfactions();

    }


    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.start();
    }

}
