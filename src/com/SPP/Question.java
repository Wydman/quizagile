package com.SPP;

import java.util.Random;

public class Question {
    private String question;
    private String reponse;
    private int pourcentageOui;

    public Question(String userQuestion, String userReponse) {
        question = userQuestion;
        reponse = userReponse;
        pourcentageOui = new Random().nextInt(100);
    }

    public int getYesPercentAnswer() {
        return pourcentageOui;
    }

    public String getQuestion() {
        return question;
    }

    public String getReponse() {
        return reponse;
    }


}
