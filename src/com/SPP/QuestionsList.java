package com.SPP;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuestionsList {
    List<Question> questionList = new ArrayList<>();
    Scanner sc = new Scanner(System.in);
    private int i = 0;

    public QuestionsList() {


    }


    public void addQuestion() {

        System.out.println("quel type de question voulez vous rentrer ? :");
        System.out.println("1)Question deroulante ");
        System.out.println("2)Question checkBox ");
        System.out.println("3)Question ouiNon ");

        switch (sc.nextInt()) {
            case 1:
                System.out.println("Veulliez rentrer une question :");
                String question = sc.nextLine();
                System.out.println("Veulliez rentrer une réponse :");
                String reponse = sc.nextLine();
                questionList.add(new QuestionDeroulante(question, reponse));
                break;
            case 2:
                System.out.println("Veulliez rentrer une question :");
                String question2 = sc.nextLine();
                System.out.println("Veulliez rentrer une réponse :");
                String reponse2 = sc.nextLine();
                questionList.add(new QuestionCheckBox(question2, reponse2));
                break;
            case 3:
                System.out.println("Veulliez rentrer une question :");
                String question3 = sc.nextLine();
                System.out.println("Veulliez rentrer une réponse :");
                String reponse3 = sc.nextLine();
                questionList.add(new QuestionOuiNon(question3, reponse3));
                break;
        }
    }

    public Question nextQuestion() {
        return questionList.get(i++);
    }

    public boolean isEmpty() {
        if (i == questionList.size()) {
            return true;
        } else {
            return false;
        }

    }
}
