package com.SPP;

import java.util.Scanner;

public class User {
    private String email;

    public String getEmail() { return this.email ;}

    public void setEmail(String email) {
        this.email = email;
    }

    public void inputEmail() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir votre mail : ");
        String str = sc.nextLine();
        this.setEmail(str);
    }
}
